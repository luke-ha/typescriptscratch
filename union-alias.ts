type Combinable = number | string;
type ConversionDescription = 'as-number' | 'as-text';

function combine(
  input1: Combinable,
  input2: Combinable,
  resultTypes: ConversionDescription
) {
  let result;
  if (
    (typeof input1 === 'number' && typeof input2 === 'number') ||
    resultTypes === 'as-number'
  ) {
    result = +input1 + +input2;
  } else {
    result = input1.toString() + input2.toString();
  }
  //   if (resultTypes === 'as-number') {
  //     return +result;
  //   } else {
  //     return result.toString();
  //   }
  return result;
}
const combinedAges = combine(23, 44, 'as-number');
console.log(combinedAges);

const combinedStringAges = combine('23', '44', 'as-number');
console.log(combinedStringAges);

const combinedNames = combine('Name', 'Anna', 'as-text');
console.log(combinedNames);
