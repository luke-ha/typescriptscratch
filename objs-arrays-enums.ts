// const person: {
//   name: string;
//   age: number;
// } = {
// const person: {
//   name: string;
//   age: number;
//   hobbies: string[];
//   role: [number, string]; //    Tupe types
// } = {
//   name: 'Ha Thang',
//   age: 30,
//   hobbies: ['Sports', 'Cooking'],
//   role: [2, 'author'],
// };

enum Role {
  ADMIN,
  READ_ONLY,
  AUTHOR,
}
const person = {
  name: 'Ha Thang',
  age: 30,
  hobbies: ['Sports', 'Cooking'],
  role: 2,
};

let favoriteActivities: string[];
favoriteActivities = ['Sports'];

// for (const hobby of person.hobbies) {
//   console.log(hobby.toUpperCase());
// }

if (person.role == Role.ADMIN) {
  console.log('Admin');
}
